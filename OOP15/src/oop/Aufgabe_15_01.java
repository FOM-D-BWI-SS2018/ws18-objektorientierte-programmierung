package oop;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Aufgabe_15_01 extends JFrame {
	
	public static void main(String[] args) {
		new Aufgabe_15_01("Hauptfenster");
	}
	
	public Aufgabe_15_01(String title) {
		super(title);
		
		setLayout(new FlowLayout());
		
		add(new JLabel("Hallo Welt"));
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 300);
		setVisible(true);
	}

}
