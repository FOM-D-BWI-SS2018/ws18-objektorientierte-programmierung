package oop;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Aufgabe_15_05 extends JFrame {

	public static void main(String[] args) {
		new Aufgabe_15_05();
	}
	
	public Aufgabe_15_05() {
		super("BorderLayout");
		setSize(400,180);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		add(new JButton("Norden"), BorderLayout.NORTH);
		add(new JButton("Süden"), BorderLayout.SOUTH);
		add(new JButton("Westen"), BorderLayout.WEST);
		add(new JButton("Osten"), BorderLayout.EAST);
		add(new RenderPanel(), BorderLayout.CENTER);
		
		setVisible(true);
	}
	
}
