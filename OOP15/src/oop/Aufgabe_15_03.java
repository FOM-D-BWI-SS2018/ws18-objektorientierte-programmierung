package oop;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Aufgabe_15_03 extends JFrame {
	
	public static void main(String[] args) {
		new Aufgabe_15_03("Komplexeres Fenster");
	}
	
	public Aufgabe_15_03(String title) {
		super(title);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		setLayout(new FlowLayout());
		
		JPanel left = new JPanel();
		left.setLayout(new GridLayout(3, 4));
		
		for (int i = 1; i <= 3; i++) {
			for (int j = 1; j < 4; j++) {
				left.add(new JButton("[ Zeile " + i + " Spalte " + j + " ]"));
			}
		}
		
		JPanel right = new JPanel();
		right.setLayout(new GridLayout(1, 2));
		
		right.add(new JLabel("FOM"));
		right.add(new JButton("Drück mich!"));
		
		add(left);
		add(right);
		
		pack();
		setVisible(true);
	}
	
}
