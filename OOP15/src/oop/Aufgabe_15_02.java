package oop;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Aufgabe_15_02 extends JFrame {
	public static void main(String[] args) {
		new Aufgabe_15_02("RenderPanel");
	}
	
	public Aufgabe_15_02(String title) {
		super(title);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(600, 300);
		
		JPanel panel = new RenderPanel();
		panel.setSize(getSize());
		add(panel);
		
		setVisible(true);
	}
}
