package oop8;

public class Zebra extends ZooTier {

	private int anazhlStreifen;

	public Zebra(String name, int gewicht, int anzahlStreifen) {
		super(name, gewicht);
		this.anazhlStreifen = anzahlStreifen;
	}

	public int getAnazhlStreifen() {
		return anazhlStreifen;
	}

}
