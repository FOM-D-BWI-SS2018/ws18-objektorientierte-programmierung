package oop8;

public class Aufgabe_08_05 {

	public static void main(String[] args) {
		Pinguin otto = new Pinguin("Otto", 400, true);
		Zebra karl = new Zebra("Karl", 459, 2);
		
		polymorpheMethode(otto);
		polymorpheMethode(karl);
	}
	
	public static void polymorpheMethode(ZooTier tier) {
		System.out.print(tier.getName() + " wiegt " + tier.getGewicht());
		
		if (tier instanceof Pinguin)
			System.out.println(" und ist " + (((Pinguin) tier).isKing() ? "ein" : "kein") + " König");
		
		if (tier instanceof Zebra)
			System.out.println(" und hat " + ((Zebra) tier).getAnazhlStreifen() + " Streifen");
		
	}
	
}
