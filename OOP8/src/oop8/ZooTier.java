package oop8;

public abstract class ZooTier {
	
	private String name;
	private int gewicht;

	public ZooTier(String name, int gewicht) {
		this.name = name;
		this.gewicht = gewicht;
	}
	
	public String getName() {
		return name;
	}

	public int getGewicht() {
		return gewicht;
	}
	
}
