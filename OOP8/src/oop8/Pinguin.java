package oop8;

public class Pinguin extends ZooTier {

	private boolean isKing;

	public Pinguin(String name, int gewicht, boolean isKing) {
		super(name, gewicht);
		this.isKing = isKing;
	}

	public boolean isKing() {
		return isKing;
	}
	
}
