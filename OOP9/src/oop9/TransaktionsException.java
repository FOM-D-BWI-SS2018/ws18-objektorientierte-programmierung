package oop9;

@SuppressWarnings("serial")
public class TransaktionsException extends Exception {
	public TransaktionsException(String message) {
		super(message);
	}
}
