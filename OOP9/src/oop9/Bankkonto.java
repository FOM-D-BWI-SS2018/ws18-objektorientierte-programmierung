package oop9;

public class Bankkonto {

	private double kontostand = 0;
	
	public double getKontostand() {
		return kontostand;
	}

	public void einzahlen(double betrag) throws TransaktionsException {
		checkNegatives(betrag);
		
		kontostand += betrag;
	}

	public void auszahlen(double betrag) throws TransaktionsException {
		checkNegatives(betrag);
		checkKontostand(betrag);
		
		kontostand -= betrag;
	}

	private void checkKontostand(double betrag) throws TransaktionsException {
		if(betrag > kontostand) {
			throw new TransaktionsException("Kontostand gibt den Auszahlungsbetrag nicht her! Geh arbeiten!");
		}
	}

	private void checkNegatives(double betrag) throws TransaktionsException {
		if(betrag < 0) {
			throw new TransaktionsException("Negative Beträge können nicht ein- oder ausgezahlt werden!");
		}
	}
	
}
