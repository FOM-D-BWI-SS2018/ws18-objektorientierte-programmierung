
public class Aufg_06_01 {

	public static void main (String[] args){
		
		int[] array = {4, 19, 20, 7, 36, 18, 1, 5};
		
		IntArray intArray = new IntArray(array);
		
		// Anzeigen
		intArray.print();
		
		// Max anzeigen
		System.out.println("Max: "+intArray.max());
		
		// Min anzeigen
		System.out.println("Min: "+intArray.min());
		
		// Mittelwert anzeigen
		System.out.println("Mittelwert: "+intArray.average());
		
		// sortieren
		intArray.sort();
		
		// Anzeigen
		intArray.print();
		
	}
	
}
