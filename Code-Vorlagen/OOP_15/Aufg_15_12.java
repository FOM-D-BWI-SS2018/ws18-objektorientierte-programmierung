import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;


public class Aufg_15_12 extends JFrame implements ActionListener {

	private JLabel pfad;
	private JButton btn;
	private JTextArea jta;
	
	

	// Konstruktor
	public Aufg_15_12(){
		super("Aufgabe 15.12");
		
		// Hier ergänzen

		
		// rows and columns als Parameter
		jta = new JTextArea(8,200);
		JScrollPane scroll = new JScrollPane();
		scroll.getViewport().add( jta );
		add(scroll, BorderLayout.CENTER);
		
		setDefaultCloseOperation (EXIT_ON_CLOSE);
		setSize(400,400);
		setVisible (true);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		
		// Hier ergänzen
		
	}

	public static void main(String[] args) {
		new Aufg_15_12();

	}
	

}

