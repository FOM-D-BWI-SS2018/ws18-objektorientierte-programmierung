package oop2;

public class HelloWorld {
	
	
	public static void main(String[] args) {
		System.out.println("Hello World");
		sayLol();
		getRandomNumber(15345);
		getRandomNumber(15);
		getRandomNumber(32);
		getRandomNumber(15323345);
		getRandomNumber(15342);
	}
	
	/**
	 * It says lol in the commandline #fcknawsome
	 */
	public static void sayLol() {
		System.out.println("lol");
	}
	
	
	/**
	 * Prints and generates a #fcknawsome random number of type int
	 * @param max maxima
	 * @return random num
	 */
	public static int getRandomNumber(int max) {
		int randomInt = (int) Math.random() * max; 
		System.out.println("Random num: " + randomInt);
		return randomInt;
	}
}
