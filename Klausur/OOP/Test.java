import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

class Guchi extends JFrame implements ActionListener {
  
  // Instanz
  JRadioButton r1, r2, r3;

  // Konstruktor
  public Guchi () {
    setLayout(new FlowLayout());

    r1 = new JRadioButton("1");
    r2 = new JRadioButton("2");
    r3 = new JRadioButton("3");
    
    ButtonGroup group = new ButtonGroup();
    group.add(r1);
    group.add(r2);
    group.add(r3);
    
    add(r1);
    add(r2);
    add(r3);

    r1.addActionListener(this);
    r2.addActionListener(this);
    r3.addActionListener(this);

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    pack();
    setVisible(true);
  }

  // Listener
  @Override
  public void actionPerformed(ActionEvent e) {
    
  }

  public static void main(String[] args) {
    new Guchi();
  }

}