import java.util.ArrayList;
import java.util.List;

class FahrzeugSimulator {

  private List<Fahrzeug> fahrzeuge;

  FahrzeugSimulator() {
    this.fahrzeuge = new ArrayList<Fahrzeug>();
  }

  void nehmeFahrzeugAuf(Fahrzeug fahrzeug) {
    fahrzeuge.add(fahrzeug);
  }

  void testeFahrfaehigkeitAllerFahrzeuge() {
    for (Fahrzeug f : fahrzeuge) {
      f.fahren();
    }
  }

  public static void main(String[] args) {
    FahrzeugSimulator simulator = new FahrzeugSimulator();
    
    simulator.nehmeFahrzeugAuf(new Lkw(400, 40000));
    simulator.nehmeFahrzeugAuf(new Auto(75, 4));

    simulator.testeFahrfaehigkeitAllerFahrzeuge();
  }

}

abstract class Fahrzeug {
  protected int ps;

  Fahrzeug(int ps) {
    this.ps = ps;
  }

  abstract void fahren();
}

class Auto extends Fahrzeug {
  private int maxAnzahlPassagiere;

  Auto(int ps, int maxAnzahlPassagiere) {
    super(ps);
    this.maxAnzahlPassagiere = maxAnzahlPassagiere;
  }

  @Override
  void fahren() {
    System.out.println("Tucker tucker tucker.. mit " + ps + "PS und maximal " + maxAnzahlPassagiere + " Passagiere");
  }
}

class Lkw extends Fahrzeug {
  private int maxGewichtDerLast;

  Lkw(int ps, int maxGewichtDerLast) {
    super(ps);
    this.maxGewichtDerLast = maxGewichtDerLast;
  }

  @Override
  void fahren() {
    System.out.println("BRRUUUMMM!! Platz da hier kommt ein " + ps + " starker Brummi fuer " + maxGewichtDerLast + " Tonnen Last!");
  }
}