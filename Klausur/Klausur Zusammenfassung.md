# Klausur OOP - Zusammenfassung

## OOP Konzepte

### Vererbung

Bei der Vererbung erbt eine Sohnklasse alle Eigenschaften (Datenfelder, Methoden) ihrer Vaterklasse und fügt ihre eigenen individuellen Eigenschaften hinzu. 

Vererbte Methoden können in der Sohnklasse überschrieben und somit verfeinert und
optimiert werden, um eine Spezialisierung zu erreichen.

Mit Vererbung können Wiederholungen im Entwurf vermieden werden. Gemeinsame Eigenschaften mehrerer Klassen werden in gemeinsame Oberklassen ausgelagert, das führt zu mehr Übersicht und zu weniger Wiederholung.

### Polymorphie

Polymorphie bedeutet Vielgestaltigkeit. Ein Objekt kann also vielgestaltig sein, d.h. es kann im Rahmen seiner Vererbungshierarchie in verschiedene Ebenen gecastet werden. 

Genau das besagt das Liskovsche Substitutionsprinzip: Ein Sohn-Objekt kann immer an die Stelle seines Vater treten. 

So kann eine polymorphe Methode redundanten Code einsparen, wenn sie als Parameter ein
Objekt einer Vaterklasse erwartet. So können sämtliche Objekte, die von dieser
Vaterklasse im Rahmen einer Vererbung abgeleitet wurden, an diese eine Methode
übergeben werden.

## Abstrakte Klassen vs. Interfaces

### Abstrakte Klasse

Eine abstrakte Klasse ist eine Klasse, die nicht direkt selbst instanziert werden kann, sie
ist vielmehr ein Teil eines Entwurfskonzeptes. Sie kann neben normalen, fertigen
Methoden auch abstrakte Methoden enthalten. Solche abstrakte Methoden werden hierbei
nur vorgegeben und müssen dann in den erbenden Klassen implementiert werden.

### Interface

Interfaces dürfen im Gegensatz zu abstrakten Klassen nur Konstanten und abstrakte
Methoden enthalten, sie dürfen außerdem keinen Konstruktor besitzen. Eine Klasse darf
von mehreren Interfaces gleichzeitig abgeleitet werden, von abstrakten Klassen darf man
nur einmal erben (keine Mehfachvererbung).

## GUI

### Layout-Manager

Layoutmanager geben Regeln vor, wie sich Komponenten in der GUI präsentieren. Sie
regeln also das Verhalten bzgl. Position, Größe und Größenveränderung insbesondere bei
Veränderungen am umgebenden Container.

#### FlowLayout

Hierbei werden die Komponenten der Reihe nach in einer Zeile hinzugefügt. Ist die Zeile voll, werden die Komponenten in die nächste Zeile positioniert. Jede Komponente hat eine eigene individuelle Größe. Standard für JPanel. 

#### GridLayout

Matrix aus Zeilen und Spalten. Alle Komponenten haben stets die gleiche Größe. Horizontale und vertikale Größenänderung wird auf die Komponenten übertragen. 

#### BorderLayout

Fünf Bereiche (NORTH, SOUTH, WEST, EAST, CENTER)
NORTH und SOUTH verändern sich horizontal, WEST und EAST nur vertikal. Der Rest wird durch CENTER aufgefüllt. Standad für JFrame. 