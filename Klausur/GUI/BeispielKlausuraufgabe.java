import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

class KlausurGUI extends JFrame implements ActionListener {

  JLabel nameLabel;
  JTextField nameTextField;
  JCheckBox vipCheckBox;
  JButton startButton;

  KlausurGUI() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLayout(new GridLayout(2, 2));

    nameLabel = new JLabel("Name");
    nameTextField = new JTextField();
    vipCheckBox = new JCheckBox("VIP");
    startButton = new JButton("Start");

    startButton.addActionListener(this);

    add(nameLabel);
    add(nameTextField);
    add(vipCheckBox);
    add(startButton);

    pack();
    setVisible(true);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String name = nameTextField.getText();
    boolean vip = vipCheckBox.isSelected();

    if (name.isEmpty()) {
      JOptionPane.showMessageDialog(null, "Bitte geben Sie einen Namen ein.", "Meldung", JOptionPane.ERROR_MESSAGE);
    } else {
      String message = name.trim() + " ist " + (vip ? "eine" : "keine") + " very important person.";

      JOptionPane.showMessageDialog(null, message, "Meldung", JOptionPane.INFORMATION_MESSAGE);
    }
  }

  public static void main(String[] args) {
    new KlausurGUI();
  }

}