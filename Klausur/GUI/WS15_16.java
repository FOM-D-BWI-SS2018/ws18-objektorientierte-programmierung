import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JLabel;

class GUI extends JFrame implements ActionListener{

  JMenuBar menuBar = new JMenuBar();
  JMenu sprachMenu = new JMenu("Sprache");
  JMenuItem deutschMenuItem = new JMenuItem("Deutch");
  JMenuItem englischMenuItem = new JMenuItem("Englisch");
  JSlider volumeSlider = new JSlider();

  GUI() {
    super("Klausur GUI");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLayout(new FlowLayout());
    
    deutschMenuItem.addActionListener(this);
    englischMenuItem.addActionListener(this);

    sprachMenu.add(deutschMenuItem);
    sprachMenu.add(englischMenuItem);
    
    menuBar.add(sprachMenu);
    setJMenuBar(menuBar);

    add(new JLabel("Lautstaerke"));
    add(volumeSlider);

    pack();
    setVisible(true);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String message = "";

    if (e.getSource() == deutschMenuItem) {
      message = "Die Lautstaerke betraegt: " + volumeSlider.getValue();
    } else if (e.getSource() == englischMenuItem) {
      message = "The value of the volume is: " + volumeSlider.getValue();
    }
    JOptionPane.showMessageDialog(null, message);
  }

  public static void main(String[] args) {
    new GUI();
  }

}