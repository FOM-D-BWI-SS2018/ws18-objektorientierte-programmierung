import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;

class MyGUI extends JFrame implements ActionListener{

  JMenuBar menuBar = new JMenuBar();
  JMenu menuFile = new JMenu("Datei");
  JMenuItem menuItemReset = new JMenuItem("Reset");
  JMenuItem menuItemClose = new JMenuItem("Schliessen");
  JTextField textName = new JTextField();
  JComboBox comboAuth = new JComboBox(new String[] {"bitte waehlen","standard", "admin"});
  JCheckBox checkKunde = new JCheckBox("Kunde");
  JButton buttonSave = new JButton("Speichern");

  MyGUI() {
    super("Meine GUI");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLayout(new GridLayout(3, 2));

    add(new JLabel("Name"));
    add(textName);
    add(new JLabel("Berechtigung"));
    add(comboAuth);
    add(checkKunde);
    add(buttonSave);

    menuItemReset.addActionListener(this);
    menuItemClose.addActionListener(this);
    buttonSave.addActionListener(this);

    menuFile.add(menuItemReset);
    menuFile.add(menuItemClose);
    menuBar.add(menuFile);
    setJMenuBar(menuBar);

    pack();
    setVisible(true);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if(e.getSource() == menuItemClose) {

      System.exit(0);

    } else if (e.getSource() == menuItemReset) {

      checkKunde.setSelected(false);
      textName.setText("");
      comboAuth.setSelectedIndex(0);

    } else if (e.getSource() == buttonSave) {

      try {
        FileOutputStream o = new FileOutputStream(new File("info.txt"));
        Properties p = new Properties();
        
        p.setProperty("name", textName.getText());
        p.setProperty("auth", comboAuth.getSelectedItem().toString());
        p.setProperty("kunde", checkKunde.isSelected() + "");
        
        p.store(o, null);
      } catch(Exception exception) {
        exception.printStackTrace();
      }

    }
  }

  public static void main(String[] args) {
    new MyGUI();
  }

}