import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.sun.prism.paint.Color;

class KomplexesFenster extends JFrame {

  KomplexesFenster() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

}

class PaintPanel extends JPanel {

  private Color background = Color.BLACK;
  private Color foreground = Color.GREEN;

  @Override
  public void paint(Graphics g) {
    super.paint(g);



  }

  public void switchColors() {
    Color temp = background;
    background = foreground;
    foreground = temp;
    
    repaint();
  }

}