package oop3;

public class AutoMain {

	public static void main(String[] args) {
		Auto a1 = new Auto("BMW", 450);
		a1.print();
		
		Auto a2 = new Auto("Audi", 440);
		a2.print();
		
		System.out.println("Auto Anzahl: " + Auto.getAnzahl());
	}

}
