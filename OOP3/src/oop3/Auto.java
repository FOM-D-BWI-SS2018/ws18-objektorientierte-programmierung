package oop3;

public class Auto {

	private String hersteller;
	private int ps;
	
	private static int anzahl;
	
	public Auto(String hersteller, int ps) {
		this.hersteller = hersteller;
		this.ps = ps;
		anzahl++;
	}

	public String getHersteller() {
		return hersteller;
	}
	
	public int getPs() {
		return ps;
	}

	public void setPs(int ps) {
		this.ps = ps;
	}


	public static int getAnzahl() {
		return anzahl;
	}
	
	public void print() {
		System.out.println("Hersteller: " + getHersteller() + " | Leistung: " + getPs() + "PS");
	}
}
